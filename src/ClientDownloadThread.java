
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 *
 *
 *
 * @author 16121961
 */
public class ClientDownloadThread implements Runnable {

    private Thread thread = null;
    private volatile boolean close = false;
    private boolean paused = false;
    private DataInputStream in = null;
    private DataOutputStream out = null;
    private ServerSocket serverSocket = null;
    private String fileName = null;
    private long fileSize;
    private Socket socket = null;
    private Client client = null;
    private String nick = null;

    public ClientDownloadThread(ServerSocket serverSocket, Client client, String nick) {
        this.serverSocket = serverSocket;
        this.client = client;
        this.nick = nick;
    }

    @Override
    public void run() {
        int [] temp;
        client.send(".down");
        try {
            socket = serverSocket.accept();
            System.out.println("Accepted socket");
        } catch (IOException ex) {
            client.msgBox("Couldn't connect to client");
        }
        FileOutputStream outF;
        openStream();
        if (validateKey()) {
            System.err.println("Validated");
            send("correct");
            setFileSizeAndName();
            client.setDownloadBarSize(fileSize);
            System.out.println(fileSize);
            try {
                outF = new FileOutputStream(fileName);
                byte b;
                long i = 0;
                boolean fill = false;
                temp = new int[50];
                int tCount = 0;
                while (i < fileSize && !close) {
                    if (!paused) {
                        b = (byte) in.read();
                        outF.write(b);
                        i++;
                        client.updateDownloadBar(i);
                        
                        if (fill && tCount < temp.length) {
                            temp[tCount++] = in.available();
                        }
                            
                        if (tCount == temp.length) { 
                            tCount = 0;
                            fill = false;
                            
                            if(isDone(temp)) {
                                client.msgBox("File incomplete");
                                break;
                            }                     
                        }
                        
                        if (i % 1000000 == 0) {
                            fill = true;
                        }    
                    }
                }
            } catch (IOException ex) {
                System.err.println("Another exception");
            }
            System.out.println("End If");
        } else {
            send("incorrect");
        }

        System.out.println("Ended");
        client.send(".downed");
    }

    public boolean isDone(int[] temp) {
        for (int i = 0; i < temp.length; i++) {
            if (temp[i] != 0) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Starts and creates the thread.
     */
    public void start() {
        if (thread == null) {
            thread = new Thread(this);
            thread.start();
        }
    }

    /**
     * Changes the value of the boolean to terminate thread execution.
     */
    public void stop() {
        close = true;
    }

    /**
     * Pause the download.
     */
    public void pauseDownload() {
        client.send(".pause" + nick);
    }

    /**
     * Resume the download.
     */
    public void resumeDownload() {
        client.send(".resume" + nick);
    }

    /**
     * Read file size from input stream.
     */
    public void setFileSizeAndName() {
        try {
            String sizeAndName = in.readUTF();
            fileSize = Long.parseLong(sizeAndName.substring(9, sizeAndName.indexOf('|')));
            fileName = sizeAndName.substring(sizeAndName.indexOf('|') + 1);
        } catch (IOException ex) {
            System.out.println("Unable to fetch filesize");
        }
    }

    /**
     * Validate key
     * 
     * @return boolean 
     */
    public boolean validateKey() {
        try {
            String tKey = in.readUTF();
            System.out.println(tKey);
            return client.validateKey(tKey);

        } catch (IOException ex) {
            System.err.println("ERER");
        }
        return false;
    }

    /**
     * Open connection streams.
     */
    public void openStream() {
        try {
            out = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
            in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
        } catch (IOException ex) {
            System.err.println("Error listening");
        }
    }

    /**
     * Send message to output stream.
     * 
     * @param msg 
     */
    public void send(String msg) {
        try {
            out.writeUTF(msg);
            out.flush();
        } catch (IOException ex) {
            System.err.println("Unable to send key");
        }
    }
}
