
import java.net.*;
import java.io.*;

public class ClientThread implements Runnable {

    private Socket socket = null;
    private Client client = null;
    private DataInputStream in = null;
    private Thread thread = null;
    private volatile boolean close = false;

    /**
     * Constructor instantiates the client and socket instance variables
     *
     * @param client
     * @param socket
     */
    public ClientThread(Client client, Socket socket) {
        this.client = client;
        this.socket = socket;
    }

    /**
     * Thread responsible for opening the input/output streams at first and then
     * handles incoming messages from the server or input channel. The input is
     * then processed but the thread also handles the specific case of an update
     * to display online users.
     *
     *
     */
    @Override
    public void run() {
        openStreams();
        while (!close) {
            try {
                String h = in.readUTF();
               
                if (h.equals(".update")) {
                    client.clearOnlineUsers();
                    int size = Integer.parseInt(in.readUTF());
                    for (int i = 0; i < size; i++) {
                        client.addOnlineUsers(in.readUTF());
                    }
                } else {
                    client.processMsg(h);
                }
            } catch (IOException ioe) {
                System.out.println("Listening error: " + ioe.getMessage());
                client.msgBox("Server error, client will now close");
                System.exit(0);
                client.stop();
            }
        }
    }

    /**
     * Opens the input stream and stops on error detection.
     */
    public void openStreams() {
        try {
            in = new DataInputStream(socket.getInputStream());
        } catch (IOException ioe) {
            System.out.println("Error getting input stream: " + ioe);
            client.stop();
        }
    }

    /**
     * Closes the input connection.
     */
    public void close() {
        try {
            if (in != null) {
                in.close();
            }
        } catch (IOException ioe) {
            System.out.println("Error closing input stream: " + ioe);
        }
    }

    /**
     * Starts and creates the thread for handling input messages
     */
    public void start() {
        if (thread == null) {
            thread = new Thread(this);
            thread.start();
        }
    }

    /**
     * Changes the boolean to true for thread termination.
     */
    public void stop() {
        close = true;
    }
}