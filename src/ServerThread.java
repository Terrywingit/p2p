
import java.net.*;
import java.io.*;

public class ServerThread implements Runnable {

    private Server server = null;
    private Socket socket = null;
    private int ID = -1;
    private DataInputStream in = null;
    private DataOutputStream out = null;
    private Thread thread = null;
    private String nick = null;
    private volatile boolean close = false;
    
    public boolean downloading;
    public boolean uploading;
    private String host = null;
    private int port = 0;
    

    /**
     * Constructor for instantiating the references and also the port number
     * used as identification.
     *
     * @param server Server object
     * @param socket Socket used by this client
     */
    public ServerThread(Server server, Socket socket) {
        this.server = server;
        this.socket = socket;
        ID = socket.getPort();
        downloading = false;
        uploading = false;
        host = socket.getInetAddress().getHostName();
        
    }

    
    /**
     * Writes the message to the output stream.
     *
     * @param msg Message
     */
    public void send(byte[] data) {
        try {
            out.write(data);
            out.flush();
        } catch (IOException ioe) {
            System.out.println(ID + "Cant send message" + ioe.getMessage());
            server.remove(ID);
            stop();
        }
    }
    
    /**
     * Writes the message to the output stream.
     *
     * @param msg Message
     */
    public void send(String msg) {
        try {
            out.writeUTF(msg);
            out.flush();
        } catch (IOException ioe) {
            System.out.println(ID + "Cant send message" + ioe.getMessage());
            server.remove(ID);
            stop();
        }
    }

    /**
     * Get the identification number for this client.
     *
     * @return ID Port number of this client
     */
    public int getID() {
        return ID;
    }

    /**
     * Thread responsible for reading the input stream and processes the message
     * after receiving it.
     */
    @Override
    public void run() {
        System.out.println("Server Thread " + ID + " running.");
        while (!close) {
            try {
                server.processRequest(ID, in.readUTF());
            } catch (IOException ioe) {
                System.out.println(ID + " ERROR reading: " + ioe.getMessage());
                server.remove(ID);
                stop();
            }
        }
    }

    /**
     * Opens the input and output streams for the client connection but expects
     * the client to send it's nickname first before anything else to set it.
     */
    public void openStreams() {
        try {
            in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
            out = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
            setNick(in.readUTF().substring(5));            
        } catch (IOException ex) {
            System.out.println("Can't open streams");
        }
    }

    /**
     * Starts and creates the thread.
     */
    public void start() {
        if (thread == null) {
            thread = new Thread(this);
            thread.start();
        }
    }

    /**
     * Changes the value of the boolean to terminate thread execution.
     */
    public void stop() {
        close = true;
    }

    /**
     * Closes all IO streams associated with this client.
     *
     * @throws IOException
     */
    public void close() throws IOException {
        if (socket != null) {
            socket.close();
        }
        if (in != null) {
            in.close();
        }
        if (out != null) {
            out.close();
        }
    }

    /**
     * Get the nickname of this client.
     *
     * @return nick Nickname
     */
    public String getNick() {
        return nick;
    }

    /**
     * Set nickname.
     * 
     * @param nickName 
     */
    public void setNick(String nickName) {
        nick = nickName;
    }

    /**
     * Get host.
     * 
     * @return String 
     */
    public String getHost() {
        return host;
    }
    
    /**
     * Get port number.
     * 
     * @return int 
     */
    public int getPort() {
        return port;
    }  
    
    /**
     * Set port number.
     * 
     * @param port 
     */
    public void setPort(int port) {
        this.port = port;
    }
    
}