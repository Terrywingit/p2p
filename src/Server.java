
import java.net.*;
import java.io.*;
import java.util.ArrayList;

public class Server implements Runnable {

    private ServerThread clients[] = new ServerThread[30];
    private ServerSocket serverSocket = null;
    private Thread thread = null;
    private int numClients = 0;
    private volatile boolean close = false;
    private ArrayList<String> nicks;

    
    /**
     * Instantiates the socket for listening and also the port for the socket.
     * The nicknames list is also created.
     *
     * @param port Port number
     */
    public Server(int port) {
        nicks = new ArrayList<>();
        try {
            System.out.println("Opening port");
            serverSocket = new ServerSocket(port);
            System.out.println("Server started: " + serverSocket);
        } catch (IOException ioe) {
            System.out.println("Can not bind to port " + port + ": " + ioe.getMessage());
        }
    }

    

    /**
     * Thread responsible for handling incoming connection requests. If
     * successful adds the client to the list of clients
     */
    @Override
    public void run() {
        while (!close) {
            try {
                System.out.println("Listening ");
                Socket client = serverSocket.accept();
                System.out.println("Client accepted");

                if (numClients < clients.length) {
                    addClient(client);
                } else {
                    System.out.println("Client refused: maximum " + clients.length + " reached.");
                }

            } catch (IOException ioe) {
                System.out.println("Server accept error: " + ioe);
                stop();
            }
        }
    }

    /**
     * Starts the thread which handles incoming client connections
     */
    public void start() {
        if (thread == null) {
            thread = new Thread(this);
            thread.start();
        }
    }

    /**
     * Changes the boolean value for thread termination
     */
    public void stop() {
        close = true;
    }

    /*
     * Gets the client index associated with this port number.
     * 
     * @param ID Port number of client
     * @return i The client index into the array
     */
    private int getClientIndex(int ID) {
        for (int i = 0; i < numClients; i++) {
            if (clients[i].getID() == ID) {
                return i;
            }
        }
        return -1;
    }

    /*
     * Gets the client index associated with this nickname.
     * 
     * @param n  nickName
     * @return i The client index into the array
     */
    private int getClientIndex(String n) {
        for (int i = 0; i < numClients; i++) {
            if (clients[i].getNick().equals(n)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Handles various cases such as an update, exit, whisper or change of user
     * list commands. The appropriate action is taken based upon the input
     * parameters given.
     *
     * @param ID Port number of client
     * @param input Message to process from client or input stream
     */
    public synchronized void processRequest(int ID, String input) {

        //System.out.println("input:" + input);
        int sPos = getClientIndex(ID);

        if (input.equals(".update")) {
            for (int i = 0; i < numClients; i++) {
                clients[i].send(".update");
                clients[i].send(nicks.size() + "");

                for (String s : nicks) {
                    clients[i].send(s);
                }
            }
        } else if (input.startsWith(".uploading")) {
            System.out.println(clients[sPos].getNick()+" uploading.");
            clients[sPos].uploading = true;
            
        } else if (input.startsWith(".uploaded")) {
            System.out.println(clients[sPos].getNick()+" finished uploading.");
            clients[sPos].uploading = false;
            
        } else if (input.equals(".down")) {
            System.out.println(clients[sPos].getNick()+" downloading.");
            clients[sPos].downloading = true;
            
        } else if (input.equals(".downed")) {
            System.out.println(clients[sPos].getNick()+" finished downloading.");
            clients[sPos].downloading = false;
            
        } else if (input.startsWith(".port")) {
                clients[sPos].setPort(Integer.parseInt(input.substring(5)));
                        
        } else if (ID == 1010101) {
            for (int i = 0; i < numClients; i++) {
                clients[i].send(input + " has left.");
            }
            System.out.println(input + " has left.");

        } else if (ID == 1010102) {
            for (int i = 0; i < numClients; i++) {
                clients[i].send(input + " has entered.");
            }
            System.out.println(input + " has entered.");
        } else if (input.startsWith(".search")) {
            String query = input.substring(7);
            System.out.println(clients[sPos].getNick()+" search for "+query);
            for (int i = 0; i < numClients; i++) {
                if (i != sPos)
                    clients[i].send(".query"+sPos+query);
            }
         
        } else if (input.startsWith(".pause")) {
            int dPos = getClientIndex(input.substring(6));
            if (dPos != -1 && clients[sPos].downloading && clients[dPos].uploading) {
                clients[dPos].send(".pause");
                System.out.println(clients[sPos].getNick()+" paused download.");
            }
        } else if (input.startsWith(".resume")) {
            int dPos = getClientIndex(input.substring(7));
            if (dPos != -1 && clients[sPos].downloading && clients[dPos].uploading) {
                clients[dPos].send(".resume");
                System.out.println(clients[sPos].getNick()+" resumed download.");
            }
            
        } else if (input.startsWith(".download")) {
            
            String targetClient = input.substring(9, input.indexOf(":"));
            int dPos = getClientIndex(targetClient);
            String fileName = input.substring((input.indexOf(":")+2), input.lastIndexOf(":"));
            String key = input.substring(input.lastIndexOf(":")+1);
            System.out.println(clients[sPos].getNick()+" request download.");
            
            if (clients[sPos].downloading) {
                clients[sPos].send("You're busy wih a download.");
            } else if (!clients[dPos].uploading) {
                clients[sPos].send(".readyDownload"+clients[dPos].getNick());
                clients[dPos].send(".info"+clients[sPos].getPort()+"|"
                        +clients[sPos].getHost()+"|"+fileName+"|"+key);
            } else {
                clients[sPos].send(targetClient + " is busy.");
            }
            
        } else if (input.startsWith(".reply")) {
            int dPos = Character.getNumericValue(input.charAt(6));
            clients[dPos].send(".sResults"+clients[sPos].getNick()+input.substring(7));
        } else if (input.startsWith(".whisper")) {

            String n = input.substring(8, input.indexOf(':'));
            int dPos = getClientIndex(n);
            System.out.println(n + dPos);
            if (dPos != -1) {
                clients[dPos].send(clients[sPos].getNick() + ":" + input.substring(input.indexOf(':') + 2));
            } else {
                clients[sPos].send("Invalid user");
            }

        } else if (input.equals(".exit")) {
            clients[sPos].send(".exit");
            remove(ID);
        } else {
            for (int i = 0; i < numClients; i++) {
                clients[i].send(clients[sPos].getNick() + ":" + input);
            }
        }
    }

    public int findClient(String host, int port) {
        for (int i = 0; i < numClients; i++) {
            if (clients[i].getHost().equals(host) && (clients[i].getPort() == port)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Removes the client associated with the ID or port number. Also calls the
     * update method for displaying refreshed list of online clients
     *
     * @param ID Port number of client
     */
    public synchronized void remove(int ID) {
        int cIndex = getClientIndex(ID);
        String n;

        if (cIndex >= 0) {
            ServerThread toRemove = clients[cIndex];
            n = toRemove.getNick();

            System.out.println("Removing client: " + n);

            /* shift clients one position down if necessary */
            if (cIndex < numClients - 1) {
                for (int i = cIndex + 1; i < numClients; i++) {
                    clients[i - 1] = clients[i];
                }
            }
            numClients--;
            try {
                toRemove.close();
            } catch (IOException ioe) {
                System.out.println("Error closing thread: " + ioe);
            }
            toRemove.stop();
            System.out.println("Client: " + ID + " removed");

            /* Request to display departed user */
            processRequest(1010101, n);
            nicks.remove(n);
            
            /* Display updated user list */
            update();

        }
    }

    /*
     * Adds the client to the array and opens it's streams.
     * Also checks for duplicate nicknames. implicitly sends
     * a notification to users of the arrival of the user.
     * 
     * @param socket Socket accepted
     */
    private void addClient(Socket socket) {

        clients[numClients] = new ServerThread(this, socket);
        clients[numClients].openStreams();
        String n = clients[numClients].getNick();

        if (nicks.contains(n)) {
            System.out.println("n");
            nicks.add(n);
            clients[numClients].send(".nexit");
            remove(clients[numClients++].getID());
        } else {
            nicks.add(n);
            clients[numClients].start();
            numClients++;
            processRequest(1010102, n);
            update();
        }

    }

    /**
     * Sends the request to show updated user list
     */
    public void update() {
        System.out.println(nicks);
        processRequest(0, ".update");
    }

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public static void main(String args[]) {
        
        
        BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
        String port = "";
        int por = 0;
        boolean incorrectInput = true;
        
        System.out.println("-------------------------------------------------");
        try {
            while (incorrectInput) {
                System.out.println("Please enter a value between 1100 and 30000:");
                
                port = r.readLine();
                if (isInteger(port)) {
                    por = Integer.parseInt(port);
                    if (por >= 1100 && por <= 30000) {
                        incorrectInput = false;
                    }
                }
                
            }
        } catch (IOException ex) {
            System.out.println("IO error and closing");
            System.exit(0);
        }
        
        System.out.println("-------------------------------------------------");
        Server s = new Server(por);
        s.start();
        System.out.println("-------------------------------------------------");
        
        while (true) {
            System.out.println("Enter stop to halt execution");
            try {
                port = r.readLine();
            } catch (IOException ex) {
                System.out.println("IO error and closing");
                s.stop();
            }

            if (port.equals("stop")) {
                s.stop();
                break;
            }
        }
        System.exit(0);
        
    }
}