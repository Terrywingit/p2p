
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.Socket;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author 16121961
 */
public class ClientUploadThread implements Runnable {

    private Thread thread = null;
    private volatile boolean close = false;
    private boolean paused = false;
    private DataOutputStream out = null;
    private DataInputStream in = null;
    private Socket socket = null;
    private File file = null;
    private String host = null;
    private int port;
    private Client client = null;
    private String key = null;

    public ClientUploadThread(File file, String host, int port, Client client, String key) {
        this.file = file;
        this.host = host;
        this.port = port;
        this.client = client;
        this.key = key;
        try {
            socket = new Socket(host, port);
        } catch (IOException ex) {
            client.msgBox("Couldn't connect to client");
        }

    }

    @Override
    public void run() {
        client.send(".uploading");
        openStream();
        sendKey();
        if (validKey()) {
            long i = 0;
            send(".fileSize" + file.length() + "|" + file.getName());
            client.setUploadBarSize(file.length());
            byte b;
            try {
                FileInputStream fi = new FileInputStream(file);
                while (!close && fi.available() != 0) {
                    if (!paused) {
                        i++;
                        //System.out.println((++i));
                        b = (byte) fi.read();
                        out.write(b);
                        client.updateUploadBar(i);
                    }
                }
                out.flush();
            } catch (IOException ex) {
                System.out.println("Error sending file");
            }
            System.out.println("Finished written " + file.length());
        }
        client.send(".uploaded");
    }

    /**
     * Starts and creates the thread.
     */
    public void start() {
        if (thread == null) {
            thread = new Thread(this);
            thread.start();
        }
    }

    /**
     * Changes the value of the boolean to terminate thread execution.
     */
    public void stop() {
        close = true;
    }

    /**
     * Send the received key.
     */
    public void sendKey() {
        send(key);
    }

    /**
     * Get validation from peer.
     * 
     * @return boolean 
     */
    public boolean validKey() {
        String ack = null;
        try {
            ack = in.readUTF();
            System.err.println(ack);
        } catch (IOException ex) {
            System.err.println("Unable to send key");
        }
        if (ack != null && ack.equals("correct")) {
            return true;
        }

        return false;
    }

    /**
     * Send message to output stream.
     * 
     * @param msg 
     */
    public void send(String msg) {
        try {
            out.writeUTF(msg);
            out.flush();
        } catch (IOException ex) {
            System.err.println("Unable to send key");
        }
    }

    /**
     * Open connection streams.
     */
    public void openStream() {
        try {
            in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
            out = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
        } catch (IOException ex) {
            System.err.println("Error listening");
        }
    }

    /**
     * Pause download.
     */
    public void pause() {
        paused = true;
    }

    /**
     * Resume download.
     */
    public void resume() {
        paused = false;
    }
}
