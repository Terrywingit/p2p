
import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client {

    private ServerSocket serverSocket = null;
    private Socket socket = null;
    private DataOutputStream out = null;
    private String nickName = null;
    private ClientGUI GUI = null;
    private ClientThread client = null;
    private ClientUploadThread uploadThread = null;
    private ClientDownloadThread downloadThread = null;

    /**
     * Constructor for initializing host, port and nickname for the client
     *
     * @param host Host machine for this client
     * @param port Port number for this client
     * @param nick Nickname for this client
     * @throws UnknownHostException Host not found
     * @throws IOException
     */
    public Client(String host, int port, String nickName) throws UnknownHostException, IOException {
        System.out.println("Connecting");
        socket = new Socket(host, port);
        serverSocket = new ServerSocket(port+2);
        //narga-o43
        System.out.println("Connected: " + socket);
        this.nickName = nickName;
    }

    /**
     * Takes the string parameter and appends it to the GUI text area. Substring
     * is used to test for exit command from server.
     *
     * @param msg
     */
    public void processMsg(String msg) {

        //System.out.println(msg);

        if (msg.startsWith(".query")) {
            send(".reply" + msg.charAt(6) + search(msg.substring(7)));
        } else if (msg.startsWith(".sResults")) {
            GUI.addSearchResults(msg);
        } else if (msg.equals(".pause")) {
            uploadThread.pause();

        } else if (msg.equals(".resume")) {
            uploadThread.resume();

        } else if (msg.startsWith(".info")) {
            String info[] = msg.substring(5).split("\\|");

            int port = Integer.parseInt(info[0]);
            String host = info[1];
            String fileName = info[2];
            String key = info[3];

            File temp = getFile(fileName);
            if (temp != null) {
                uploadThread = new ClientUploadThread(temp, host, port, this, key);
                uploadThread.start();
            }

            //System.err.println(port + "|" + host + "|" + fileName + "|" + key);
        } else if (msg.startsWith(".readyDownload")) {
            startDownloadThread(msg.substring(14));
            
        } else if (msg.equals(".nexit")) {
            msgBox("Nick name taken, application will now close");
            closeConnections();
        } else if (msg.equals(".exit")) {
            closeConnections();
        } else {
            GUI.addMessage(msg + "\n");
        }
    }

    /**
     * Opens the output stream and starts the thread for handling input. Also
     * sends nickname to the server before anything else.
     *
     * @throws IOException
     */
    public void start() throws IOException {
        out = new DataOutputStream(socket.getOutputStream());

        /* Send nick first */
        send(".nick" + nickName);

        /* Send socket listened on */
        send(".port" + serverSocket.getLocalPort());

        client = new ClientThread(this, socket);
        client.start();
    }

    /**
     * Sends the message along with the added nickname into the output stream
     *
     * @param msg Message to be sent
     */
    public void send(byte[] data) {
        try {
            out.write(data);
            out.flush();
        } catch (IOException ex) {
            System.out.println("Cant send data");
        }
    }

    /**
     * Sends the message along with the added nickname into the output stream
     *
     * @param msg Message to be sent
     */
    public void send(String msg) {
        try {
            out.writeUTF(msg);
            out.flush();
        } catch (IOException ex) {
            System.out.println("Cant send " + msg);
        }
    }

    /**
     * Closes the client connections
     * 
     */
    public void closeConnections() {
        if (client != null) {
            client.stop();
            client.close();
        }

        if (out != null) {
            try {
                out.close();
            } catch (IOException ex) {                
            }
        }
        
        if (serverSocket != null) {
            try {
                serverSocket.close();
            } catch (IOException ex) {
                Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.exit(0);
    }

    /**
     * Closes all open connections and stops the running thread. Also exits
     * afterward
     */
    public void stop() {
        if (downloadThread != null) 
            downloadThread.stop();
        
        if (uploadThread != null) 
            uploadThread.stop();
      
        send(".exit");
    }

    /**
     * Instantiate this client's nickname
     *
     * @param nick Nickname
     */
    public void setNick(String nick) {
        nickName = nick;
    }

    /**
     * Appends the nickname to the text area on the GUI
     *
     * @param nick Nickname
     */
    public void addOnlineUsers(String nick) {
        GUI.addOnlineUsers(nick);
    }

    /**
     * Clears the GUI user log
     */
    public void clearOnlineUsers() {
        GUI.clearUserLog();
    }

    public void setGUI(ClientGUI clientGUI) {
        GUI = clientGUI;
    }

    /**
     * Calls a dialog box to display message
     *
     * @param msg message
     */
    public void msgBox(String msg) {
        GUI.msgBox(msg);
    }

    public File getFile(String fileName) {
        ArrayList<File> l = GUI.getSharedFiles();
        if (!l.isEmpty()) {
            for (int i = 0; i < l.size(); i++) {
                if (l.get(i).getName().equals(fileName)) {
                    return l.get(i);
                }
            }
        }
        return null;
    }

    /**
     * Search the shared files list for a matching string.
     * 
     * @param fileName
     * @return String 
     */
    public String search(String fileName) {

        String files = "";
        ArrayList<File> l = GUI.getSharedFiles();
        if (!l.isEmpty()) {
            for (int i = 0; i < l.size(); i++) {
                if (l.get(i).getName().toLowerCase().contains(fileName.toLowerCase())) {
                    files += "|" + l.get(i).getName();
                }
            }
        }
        if (files.equals("")) {
            files += "|none";
        }
        return files;
    }

    /**
     * Pause the download thread.
     */
    public void pauseDownload() {
        if (downloadThread != null) {
            downloadThread.pauseDownload();
        }
    }

    /**
     * Resume the download.
     */
    public void resumeDownload() {
        if (downloadThread != null) {
            downloadThread.resumeDownload();
        }
    }

    /**
     * Start the download thread.
     */
    public void startDownloadThread(String nick) {
        downloadThread = new ClientDownloadThread(serverSocket, this, nick);
        downloadThread.start();

    }
    
    
    /**
     * Set the bar size.
     * 
     * @param size 
     */
    public void setDownloadBarSize(long size) {
        GUI.setDownloadBarSize(size);
    }

    /**
     * Update bar progress. 
     * 
     * @param i 
     */
    public void updateDownloadBar(long i) {
        GUI.updateDownloadBar(i);
    }

     /**
     * Set the bar size.
     * 
     * @param size 
     */
    public void setUploadBarSize(long size) {
        GUI.setUploadBarSize(size);
    }

     /**
     * Update bar progress. 
     * 
     * @param i 
     */
    public void updateUploadBar(long i) {
        GUI.updateUploadBar(i);
    }

    /**
     * Check whether key is valid.
     * 
     * @param key
     * @return boolean 
     */
    public boolean validateKey(String key) {
        return GUI.validateKey(key);
    }

    public static void main(String args[]) throws IOException {
        Client client = null;

        client = new Client("o39.narga.sun.ac.za.", 5000, "joe");
        client.start();
    }
}